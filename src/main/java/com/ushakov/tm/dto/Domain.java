package com.ushakov.tm.dto;

import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.model.User;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    @Nullable
    private List<User> users;

    @Nullable
    private List<Project> projects;

    @Nullable
    private List<Task> tasks;

}