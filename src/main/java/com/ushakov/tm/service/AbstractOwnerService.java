package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IOwnerRepository;
import com.ushakov.tm.api.service.IOwnerService;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyUserIdException;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity>
        extends AbstractService<E>
        implements IOwnerService<E> {

    @NotNull
    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(@NotNull final IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Override
    @Nullable
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable List<E> entities = repository.findAll(userId);
        if (entities == null) throw new ObjectNotFoundException();
        return entities;
    }

    @Override
    @Nullable
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) throw new ObjectNotFoundException();
        return repository.findAll(userId, comparator);
    }

    @Override
    @NotNull
    public E findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = repository.findOneById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        return entity;
    }

    @Override
    @Nullable
    public E removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeOneById(userId, id);
    }

    @Override
    @NotNull
    public E findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable final E entity = repository.findOneByIndex(userId, index);
        if (entity == null) throw new ObjectNotFoundException();
        return entity;
    }

    @Override
    @Nullable
    public E removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeOneByIndex(userId, index);
    }

}