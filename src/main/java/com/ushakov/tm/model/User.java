package com.ushakov.tm.model;

import com.ushakov.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    @Override
    @NotNull
    public String toString() {
        return "ID: " + id + "\nFULL NAME: " + firstName + " " + lastName + " " + middleName
                + "\nLOGIN" + login + "\nEMAIL: " + email + "\nROLE:" + role.getDisplayName();
    }

}
