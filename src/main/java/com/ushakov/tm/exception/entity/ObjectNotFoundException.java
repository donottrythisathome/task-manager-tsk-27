package com.ushakov.tm.exception.entity;

import com.ushakov.tm.exception.AbstractException;

public class ObjectNotFoundException extends AbstractException {

    public ObjectNotFoundException() {
        super("Error! Object was not found!");
    }

}