package com.ushakov.tm.exception.empty;

import com.ushakov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty!");
    }

}
