package com.ushakov.tm.exception.empty;

import com.ushakov.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty!");
    }

}
