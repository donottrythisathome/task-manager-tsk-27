package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Find all tasks related to project.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> projectTaskList = serviceLocator.getProjectTaskService().findAllTasksByProjectId(userId, projectId);
        if (projectTaskList == null) throw new TaskNotFoundException();
        int index = 1;
        for (@NotNull final Task task : projectTaskList) {
            System.out.println(index + ": " + task);
            System.out.println();
            index++;
        }
    }

    @Override
    @NotNull
    public String name() {
        return "find-tasks-by-project-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
