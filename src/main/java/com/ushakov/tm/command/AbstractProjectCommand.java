package com.ushakov.tm.command;

import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Start Date: " + project.getDateStart());
        System.out.println("Finish Date: " + project.getDateEnd());
        System.out.println("Created: " + project.getCreated());
    }

}
