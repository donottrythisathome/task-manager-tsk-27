package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Change project status by name.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT NAME");
        @NotNull final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Project project = serviceLocator.getProjectService().changeStatusByName(userId, projectName, status);
    }

    @Override
    @NotNull
    public String name() {
        return "change-project-status-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
