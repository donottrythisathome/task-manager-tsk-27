package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Unlock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockByLogin(login);
        System.out.println("OK");
    }

    @Override
    @NotNull
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
