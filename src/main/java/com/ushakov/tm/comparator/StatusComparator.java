package com.ushakov.tm.comparator;

import com.ushakov.tm.api.model.IHasStatus;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

@NoArgsConstructor
public final class StatusComparator implements Comparator<IHasStatus> {

    @NotNull
    private static final StatusComparator INSTANCE = new StatusComparator();

    @NotNull
    public static StatusComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasStatus o1, @Nullable final IHasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
