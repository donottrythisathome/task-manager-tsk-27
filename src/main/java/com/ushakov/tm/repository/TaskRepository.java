package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerBusinessRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        List<Task> list = findAll(userId);
        Optional.ofNullable(list).orElseThrow(ObjectNotFoundException::new);
        return list.stream()
                .filter(t -> projectId.equals(t.getProjectId()))
                .collect(Collectors.toList());
    }

}
